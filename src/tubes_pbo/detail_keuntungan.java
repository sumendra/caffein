/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes_pbo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author LENOVO
 */
public class detail_keuntungan extends javax.swing.JFrame {
Connection con;
    Statement state;
    ResultSet rs;
    String sql;
    /**
     * Creates new form detail_keuntungan
     */
    String kode_transaksi;
    String id_makanan;
    String keun;
    ImageIcon imageIcon;
    String username;
    public detail_keuntungan(String idtransaksi,String kodemakanan,String keuntungan,String user) {
        initComponents();
          imageIcon= new ImageIcon("src/gambar/Untitled-1.png");
        setIconImage(imageIcon.getImage());
        kode_transaksi=idtransaksi;
        id_makanan=kodemakanan;
         konfig db = new konfig();
        db.config();
        con=db.con;
        state=db.stm;
        username=user;
        keun=keuntungan;
        tampil_detail();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        hargaakhir = new javax.swing.JLabel();
        hargaawal = new javax.swing.JLabel();
        waktu = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        kuantitas = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        id_transaksi = new javax.swing.JLabel();
        kode_makanan = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        kode_pegawai = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        keuntungan = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(232, 234, 246));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("tubes_pbo/Bundle"); // NOI18N
        jLabel1.setText(bundle.getString("detail_keuntungan.jLabel1.text")); // NOI18N

        jLabel7.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel7.setText(bundle.getString("detail_keuntungan.jLabel7.text")); // NOI18N

        jLabel5.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel5.setText(bundle.getString("detail_keuntungan.jLabel5.text")); // NOI18N

        hargaakhir.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        hargaakhir.setText(bundle.getString("detail_keuntungan.hargaakhir.text")); // NOI18N

        hargaawal.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        hargaawal.setText(bundle.getString("detail_keuntungan.hargaawal.text")); // NOI18N

        waktu.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        waktu.setText(bundle.getString("detail_keuntungan.waktu.text")); // NOI18N

        jLabel4.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel4.setText(bundle.getString("detail_keuntungan.jLabel4.text")); // NOI18N

        kuantitas.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        kuantitas.setText(bundle.getString("detail_keuntungan.kuantitas.text")); // NOI18N

        jLabel3.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel3.setText(bundle.getString("detail_keuntungan.jLabel3.text")); // NOI18N

        id_transaksi.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        id_transaksi.setText(bundle.getString("detail_keuntungan.id_transaksi.text")); // NOI18N

        kode_makanan.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        kode_makanan.setText(bundle.getString("detail_keuntungan.kode_makanan.text")); // NOI18N

        jLabel9.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel9.setText(bundle.getString("detail_keuntungan.jLabel9.text")); // NOI18N

        kode_pegawai.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        kode_pegawai.setText(bundle.getString("detail_keuntungan.kode_pegawai.text")); // NOI18N

        jLabel8.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel8.setText(bundle.getString("detail_keuntungan.jLabel8.text")); // NOI18N

        jLabel6.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        jLabel6.setText(bundle.getString("detail_keuntungan.jLabel6.text")); // NOI18N

        keuntungan.setFont(new java.awt.Font("Calibri Light", 0, 18)); // NOI18N
        keuntungan.setText(bundle.getString("detail_keuntungan.keuntungan.text")); // NOI18N

        jLabel10.setText(bundle.getString("detail_keuntungan.jLabel10.text")); // NOI18N

        jLabel11.setText(bundle.getString("detail_keuntungan.jLabel11.text")); // NOI18N

        jLabel12.setText(bundle.getString("detail_keuntungan.jLabel12.text")); // NOI18N

        jLabel13.setText(bundle.getString("detail_keuntungan.jLabel13.text")); // NOI18N

        jLabel15.setText(bundle.getString("detail_keuntungan.jLabel15.text")); // NOI18N

        jLabel16.setText(bundle.getString("detail_keuntungan.jLabel16.text")); // NOI18N

        jLabel17.setText(bundle.getString("detail_keuntungan.jLabel17.text")); // NOI18N

        jLabel20.setText(bundle.getString("detail_keuntungan.jLabel20.text")); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel16))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel17))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel20))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel15))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel13))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel12))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                                .addComponent(jLabel11)))
                        .addGap(18, 18, 18)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(keuntungan)
                    .addComponent(hargaakhir)
                    .addComponent(hargaawal)
                    .addComponent(kode_pegawai)
                    .addComponent(waktu)
                    .addComponent(kuantitas)
                    .addComponent(id_transaksi)
                    .addComponent(kode_makanan))
                .addContainerGap(186, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(kode_pegawai)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel11)
                    .addComponent(kode_makanan))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(id_transaksi)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(kuantitas)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(waktu)
                    .addComponent(jLabel15))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(hargaawal)
                    .addComponent(jLabel20))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(hargaakhir)
                    .addComponent(jLabel17))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(keuntungan))
                    .addComponent(jLabel16))
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("Calibri Light", 0, 24)); // NOI18N
        jLabel2.setText(bundle.getString("detail_keuntungan.jLabel2.text")); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel2)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        halamanutamaAdmin utama = new halamanutamaAdmin(username);
        utama.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_formWindowClosing
    public void tampil_detail(){
        try{
        sql="select * from daftar_makanan where kode_makanan='"+id_makanan+"'";
        rs=state.executeQuery(sql);
        while(rs.next()){
            String hargaawa=rs.getString("harga_awal");
            String hargaakhi=rs.getString("harga");
            
            String query="select * from transaksi where id_transaksi='"+kode_transaksi+"'";
            rs=state.executeQuery(query);
            while(rs.next()){
               String kodepegawai=rs.getString("kode_pegawai");
               String kuatitas=rs.getString("jumlah");
               String wakt=rs.getString("tanggal");
               kode_pegawai.setText(kodepegawai);
               kode_makanan.setText(id_makanan);
               id_transaksi.setText(kode_transaksi);
               kuantitas.setText(kuatitas);
               waktu.setText(wakt);
               hargaawal.setText(hargaawa);
               hargaakhir.setText(hargaakhi);
               keuntungan.setText(keun);
            }
        }
        }
        catch(Exception e){
                JOptionPane.showMessageDialog(this, e.getMessage());
                }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(detail_keuntungan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(detail_keuntungan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(detail_keuntungan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(detail_keuntungan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
       
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel hargaakhir;
    private javax.swing.JLabel hargaawal;
    private javax.swing.JLabel id_transaksi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel keuntungan;
    private javax.swing.JLabel kode_makanan;
    private javax.swing.JLabel kode_pegawai;
    private javax.swing.JLabel kuantitas;
    private javax.swing.JLabel waktu;
    // End of variables declaration//GEN-END:variables
}
