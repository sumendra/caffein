-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2019 at 04:12 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caffein`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `kode_admin` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `gaji` int(100) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`kode_admin`, `nama`, `password`, `alamat`, `gaji`, `jenis_kelamin`, `jabatan`, `waktu`) VALUES
(10, 'sumendra', 'sumendra', 'bali', 20000000, 'laki-laki', 'Admin Internal', '2018-11-19 21:10:35'),
(11, 'averin', 'averin', 'jatim', 0, 'perempuan', 'Admin External', '2018-12-04 23:00:40');

-- --------------------------------------------------------

--
-- Table structure for table `bantuan_transaksi`
--

CREATE TABLE `bantuan_transaksi` (
  `id_bantuan` int(10) NOT NULL,
  `penjelasan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftar_gaji`
--

CREATE TABLE `daftar_gaji` (
  `id_daftargaji` int(10) NOT NULL,
  `daftar_gaji` int(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_gaji`
--

INSERT INTO `daftar_gaji` (`id_daftargaji`, `daftar_gaji`, `jabatan`) VALUES
(1, 60000, 'parkir'),
(2, 160000, 'manager'),
(3, 100000, 'cleaning service'),
(4, 120000, 'weater'),
(5, 120000, 'weatris');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_kehadiran`
--

CREATE TABLE `daftar_kehadiran` (
  `id_kehadiran` int(10) NOT NULL,
  `jumlah_kehadiran` int(10) NOT NULL,
  `tanggal` varchar(255) NOT NULL,
  `status_kehadiran` varchar(100) NOT NULL,
  `kode_pegawai` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_kehadiran`
--

INSERT INTO `daftar_kehadiran` (`id_kehadiran`, `jumlah_kehadiran`, `tanggal`, `status_kehadiran`, `kode_pegawai`) VALUES
(26, 3, '2018/12/04', 'tidak hadir', 7),
(27, 1, '2018/12/05', 'tidak hadir', 6);

-- --------------------------------------------------------

--
-- Table structure for table `daftar_makanan`
--

CREATE TABLE `daftar_makanan` (
  `kode_makanan` int(10) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `stok` int(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `diskon` int(100) NOT NULL,
  `harga_awal` int(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `kode_admin` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_makanan`
--

INSERT INTO `daftar_makanan` (`kode_makanan`, `nama_produk`, `stok`, `harga`, `diskon`, `harga_awal`, `keterangan`, `kode_admin`) VALUES
(14, 'Aqua', 91, 3000, 0, 2500, 'ADA', 10),
(15, 'Ayam Bakar', 81, 20000, 0, 10000, 'ADA', 10),
(16, 'Nasi Putih', 31, 4000, 0, 2000, 'ADA', 10),
(17, 'Ayam Goreng', 98, 25000, 2, 8000, 'ADA', 10);

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `kode_gaji` int(10) NOT NULL,
  `gaji` int(100) NOT NULL,
  `id_kehadiran` int(10) NOT NULL,
  `id_daftargaji` int(10) NOT NULL,
  `kode_pegawai` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`kode_gaji`, `gaji`, `id_kehadiran`, `id_daftargaji`, `kode_pegawai`) VALUES
(10, 499998, 26, 2, 7),
(11, 133333, 27, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `keuntungan`
--

CREATE TABLE `keuntungan` (
  `id_keuntungan` int(10) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keuntungan` int(100) NOT NULL,
  `id_transaksi` int(10) NOT NULL,
  `kode_makanan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keuntungan`
--

INSERT INTO `keuntungan` (`id_keuntungan`, `waktu`, `keuntungan`, `id_transaksi`, `kode_makanan`) VALUES
(1, '2018-12-05 09:30:45', 2000, 1, 16),
(2, '2018-12-05 09:30:53', 17000, 2, 17),
(3, '2018-12-05 15:21:49', 0, 3, 15),
(4, '2018-12-05 15:21:57', 500, 4, 17),
(5, '2018-12-05 15:22:11', 0, 5, 14),
(6, '2018-12-05 15:22:18', 0, 6, 16);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id_pesan` int(10) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isi` varchar(999) NOT NULL,
  `kode_pegawai` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id_pesan`, `judul`, `tanggal`, `isi`, `kode_pegawai`) VALUES
(1, 'Terdapat protes ', '2018-11-20 09:55:53', 'tadi ada seorang pelanggan yang protes berkaitan keran di denpan ', 7),
(3, 'aqua habis', '2018-11-16 13:36:07', 'stok aqua habis dalam waktu perkiraan hari ini', 7);

-- --------------------------------------------------------

--
-- Table structure for table `message_pegawai`
--

CREATE TABLE `message_pegawai` (
  `id_messagepegawai` int(10) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isi` varchar(999) NOT NULL,
  `kode_admin` int(10) NOT NULL,
  `kode_pegawai` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_pegawai`
--

INSERT INTO `message_pegawai` (`id_messagepegawai`, `judul`, `tanggal`, `isi`, `kode_admin`, `kode_pegawai`) VALUES
(1, 'Terdapat protes ', '2018-12-05 15:16:25', 'dddd', 10, 7),
(2, 'balasan berkaitan pesan keran ', '2018-11-20 15:09:40', 'nanti tolong kamu cari tukang  untuk perbaiki dan temui saya besok jam 12 ', 10, 7),
(3, 'aqua habis', '2018-12-04 23:08:46', 'okok nanti saya belikan ', 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `kode_pegawai` int(10) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `password_pegawai` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `gaji` int(100) NOT NULL,
  `jenis_kelamin` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `waktu` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`kode_pegawai`, `nama_pegawai`, `password_pegawai`, `alamat`, `gaji`, `jenis_kelamin`, `jabatan`, `waktu`) VALUES
(6, 'averin', ' averin', ' jatim', 133333, 'perempuan', 'Weatris', '2018-12-04 23:01:28'),
(7, 'kadek', 'kadek', 'bali', 133333, 'laki-laki', 'Manager', '2018-12-02 13:58:42');

-- --------------------------------------------------------

--
-- Table structure for table `tampung_transaksi`
--

CREATE TABLE `tampung_transaksi` (
  `id_tampungtransaksi` int(11) NOT NULL,
  `kode_makanan` int(10) NOT NULL,
  `id_transaksi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(10) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `diskon` int(100) NOT NULL,
  `pemasukan` int(100) NOT NULL,
  `kode_pegawai` int(10) NOT NULL,
  `kode_makanan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `jumlah`, `tanggal`, `diskon`, `pemasukan`, `kode_pegawai`, `kode_makanan`) VALUES
(1, 1, '2018-12-05 09:30:45', 0, 4000, 6, 16),
(2, 1, '2018-12-05 09:30:53', 2, 24500, 6, 17),
(3, 1, '2018-12-05 15:21:49', 0, 20000, 6, 15),
(4, 1, '2018-12-05 15:21:57', 2, 24500, 6, 17),
(5, 1, '2018-12-05 15:22:11', 0, 3000, 6, 14),
(6, 1, '2018-12-05 15:22:18', 0, 4000, 6, 16);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`kode_admin`);

--
-- Indexes for table `bantuan_transaksi`
--
ALTER TABLE `bantuan_transaksi`
  ADD PRIMARY KEY (`id_bantuan`);

--
-- Indexes for table `daftar_gaji`
--
ALTER TABLE `daftar_gaji`
  ADD PRIMARY KEY (`id_daftargaji`);

--
-- Indexes for table `daftar_kehadiran`
--
ALTER TABLE `daftar_kehadiran`
  ADD PRIMARY KEY (`id_kehadiran`),
  ADD KEY `kode_pegawai` (`kode_pegawai`);

--
-- Indexes for table `daftar_makanan`
--
ALTER TABLE `daftar_makanan`
  ADD PRIMARY KEY (`kode_makanan`),
  ADD KEY `kode_admin` (`kode_admin`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`kode_gaji`),
  ADD KEY `id_kehadiran` (`id_kehadiran`),
  ADD KEY `id_daftargaji` (`id_daftargaji`),
  ADD KEY `id_pegawai` (`kode_pegawai`);

--
-- Indexes for table `keuntungan`
--
ALTER TABLE `keuntungan`
  ADD PRIMARY KEY (`id_keuntungan`),
  ADD KEY `id_transaksi` (`id_transaksi`),
  ADD KEY `kode_makanan` (`kode_makanan`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_pesan`),
  ADD KEY `kode_pegawai` (`kode_pegawai`);

--
-- Indexes for table `message_pegawai`
--
ALTER TABLE `message_pegawai`
  ADD PRIMARY KEY (`id_messagepegawai`),
  ADD KEY `kode_admin` (`kode_admin`),
  ADD KEY `kode_pegawai` (`kode_pegawai`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`kode_pegawai`);

--
-- Indexes for table `tampung_transaksi`
--
ALTER TABLE `tampung_transaksi`
  ADD PRIMARY KEY (`id_tampungtransaksi`),
  ADD KEY `id_transaksi` (`id_transaksi`),
  ADD KEY `kode_makanan` (`kode_makanan`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `kode_pegawai` (`kode_pegawai`),
  ADD KEY `kode_admin` (`kode_makanan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `kode_admin` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `daftar_gaji`
--
ALTER TABLE `daftar_gaji`
  MODIFY `id_daftargaji` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `daftar_kehadiran`
--
ALTER TABLE `daftar_kehadiran`
  MODIFY `id_kehadiran` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `daftar_makanan`
--
ALTER TABLE `daftar_makanan`
  MODIFY `kode_makanan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `kode_gaji` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `keuntungan`
--
ALTER TABLE `keuntungan`
  MODIFY `id_keuntungan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tampung_transaksi`
--
ALTER TABLE `tampung_transaksi`
  MODIFY `id_tampungtransaksi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `keuntungan`
--
ALTER TABLE `keuntungan`
  ADD CONSTRAINT `keuntungan_ibfk_1` FOREIGN KEY (`kode_makanan`) REFERENCES `daftar_makanan` (`kode_makanan`);

--
-- Constraints for table `message_pegawai`
--
ALTER TABLE `message_pegawai`
  ADD CONSTRAINT `message_pegawai_ibfk_2` FOREIGN KEY (`kode_admin`) REFERENCES `admin` (`kode_admin`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
